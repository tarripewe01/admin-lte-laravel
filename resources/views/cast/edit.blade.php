@extends('master')

@section('title')
    Halaman Edit Cast
@endsection

@section('content')

    <div class="ml-3 mt-3 mr-15">
        <form action="/cast/{{$cast->id}}" method="POST">
        @method('put')
            @csrf
            <div class="form-group">
                <label>Nama Pemeran</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" placeholder="Isi Nama Pemeran">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label>Umur Pemeran</label>
                <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" placeholder="Isi Umur Pemeran">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label>Biodata Pemeran</label>
                <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" placeholder="Isi Umur Pemeran">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
    </div>

@endsection